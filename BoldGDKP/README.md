# BoldGDKP
Garbage addon for WoW 1.12.1 clients with misc functions.

## Commands

### /bgd (help)
Show help.

### /bgd ls
Show current entries.

### /bgd 100 Name [ItemLink]
Add new entry.

### /bgd rm 1
Remove entry 1.

### /bgd clear
Clear all entries.
